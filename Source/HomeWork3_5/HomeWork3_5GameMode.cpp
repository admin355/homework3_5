// Copyright Epic Games, Inc. All Rights Reserved.

#include "HomeWork3_5GameMode.h"
#include "HomeWork3_5HUD.h"
#include "HomeWork3_5Character.h"
#include "UObject/ConstructorHelpers.h"

AHomeWork3_5GameMode::AHomeWork3_5GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AHomeWork3_5HUD::StaticClass();
}
