// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class HomeWork3_5 : ModuleRules
{
	public HomeWork3_5(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
