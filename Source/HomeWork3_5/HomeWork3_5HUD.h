// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "HomeWork3_5HUD.generated.h"

UCLASS()
class AHomeWork3_5HUD : public AHUD
{
	GENERATED_BODY()

public:
	AHomeWork3_5HUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

