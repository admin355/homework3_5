// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HomeWork3_5GameMode.generated.h"

UCLASS(minimalapi)
class AHomeWork3_5GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AHomeWork3_5GameMode();
};



